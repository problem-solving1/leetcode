// O(N) algorithm by using Kadane's algorithm for max-sum and min-sum

/**
 * @param {number[]} A
 * @return {number}
 */
var maxSubarraySumCircular = function (A) {
  let min_subset_sum = 0;
  let min_subset = 0;
  let max_subset_sum = 0;
  let max_subset = 0;
  let maxDigit = Number.MIN_SAFE_INTEGER;
  let sum = 0;

  A.forEach((digit) => {
    sum += digit;
    if (maxDigit < digit) {
      maxDigit = digit;
    }

    // calculate minimum subset
    if (min_subset_sum + digit > 0) {
      min_subset_sum = 0;
    } else {
      min_subset_sum = min_subset_sum + digit;
      if (min_subset_sum < min_subset) {
        min_subset = min_subset_sum;
      }
    }

    // calculate maximum subset
    if (max_subset_sum + digit < 0) {
      max_subset_sum = 0;
    } else {
      max_subset_sum = max_subset_sum + digit;
      if (max_subset_sum > max_subset) {
        max_subset = max_subset_sum;
      }
    }
  });

  let answer = Math.max(max_subset, sum - min_subset);
  if (answer !== 0) {
    return answer;
  } else return maxDigit;
};

console.log(maxSubarraySumCircular([-5, -2, -3]));
console.log(maxSubarraySumCircular([1, -2, 3, -2]));
console.log(maxSubarraySumCircular([5, -3, 5]));
console.log(maxSubarraySumCircular([3, -1, 2, -1]));
console.log(maxSubarraySumCircular([3, -2, 2, -3]));
console.log(maxSubarraySumCircular([-2, -3, -1]));
