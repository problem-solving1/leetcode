/**
 * O(N) algorithm
 * @param {number[]} flowerbed
 * @param {number} n
 * @return {boolean}
 */
var canPlaceFlowers = function (flowerbed, n) {
  let currentPosition = 0;
  let placedFlowerCount = 0;
  while (currentPosition < flowerbed.length && placedFlowerCount < n) {
    // if current place has no flower and if there's no adjacent flowers, plant a flower
    if (
      !flowerbed[currentPosition] &&
      !flowerbed[currentPosition - 1] &&
      !flowerbed[currentPosition + 1]
    ) {
      flowerbed[currentPosition] = 1;
      placedFlowerCount++;
    }

    if (flowerbed[currentPosition]) {
      currentPosition += 2;
    } else {
      currentPosition++;
    }
  }

  return placedFlowerCount === n;
};

console.log(canPlaceFlowers([1,0,0,0,1], 2));
