/**
 * @param {number[]} nums
 * @return {number}
 */
var thirdMax = function (nums) {
  let firstMax, secondMax, thirdMax;
  firstMax = secondMax = thirdMax = Number.MIN_SAFE_INTEGER;

  for (let num of nums) {
    if (firstMax === num || secondMax === num || thirdMax === num) {
      continue;
    } else if (num > firstMax) {
      [firstMax, secondMax, thirdMax] = [num, firstMax, secondMax];
    } else if (num > secondMax) {
      [secondMax, thirdMax] = [num, secondMax];
    } else if (num > thirdMax) {
      thirdMax = num;
    }
  }

  return thirdMax !== Number.MIN_SAFE_INTEGER ? thirdMax : Math.max(...nums);
};

// debugger;
console.log(thirdMax([1, 2, -2147483648]));
// console.log(Number.MIN_SAFE_INTEGER);
