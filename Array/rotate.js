const reverse = (array, start, end) => {
    while (start <= end) {
        let temp = array[start];
        array[start] = array[end];
        array[end] = temp;

        start++;
        end--;
    }
}

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var rotate = function (nums, k) {
    k %= nums.length;

    reverse(nums, 0, nums.length - 1);
    reverse(nums, 0, k - 1);
    reverse(nums, k, nums.length - 1);

    return nums;
};

console.log(rotate([1, 2, 3, 4, 5, 6, 7], 3));
console.log(rotate([-1, -100, 3, 99], 2));
console.log(rotate([-1], 2));
console.log(rotate([1, 2], 3));
console.log(rotate([1, 2, 3], 4));