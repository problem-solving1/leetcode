/**
 * O(N) time complexity
 * @param {number[]} arr
 * @return {boolean}
 */
var threeConsecutiveOdds = function (arr) {
  let count = 0;
  let threeConsecutiveOdds = false;
  arr.forEach((num, index) => {
    if (num % 2 == 0) count = 0;
    else count++;

    if (count === 3) {
      index = arr.length;
      threeConsecutiveOdds = true;
    }
  });

  return threeConsecutiveOdds;
};

console.log(threeConsecutiveOdds([2, 6, 4, 1]));
console.log(threeConsecutiveOdds([1, 2, 34, 3, 4, 5, 7, 23, 12]));
console.log(threeConsecutiveOdds([1, 3]));
