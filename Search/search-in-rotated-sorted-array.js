const findPivot = (nums) => {
  let start = 0,
    end = nums.length - 1;
  while (start < end) {
    let mid = start + Math.floor((end - start) / 2);
    if (nums[mid] > nums[end]) {
      start = mid + 1;
    } else {
      end = mid;
    }
  }

  return start;
};

const binarySearch = (nums, target, start, end) => {
  let index;
  while (start <= end) {
    let mid = start + Math.floor((end - start) / 2);
    if (nums[mid] === target) {
      index = mid;
      break;
    }

    nums[mid] < target ? (start = mid + 1) : (end = mid - 1);
  }

  return index;
};

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function (nums, target) {
  debugger;
  let pivot = findPivot(nums);

  let index = binarySearch(nums, target, 0, pivot - 1);
  if (index !== undefined) return index;
  index = binarySearch(nums, target, pivot, nums.length - 1);
  if (index !== undefined) return index;
  return -1;
};

console.log(search([5,1,3], 5));
