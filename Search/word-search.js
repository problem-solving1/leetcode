const directions = [
  [-1, 0],
  [0, -1],
  [1, 0],
  [0, 1],
];

const isWithInBounds = (row, col, rowMax, colMax) => {
  return row >= 0 && col >= 0 && row < rowMax && col < colMax;
};

/**
 *
 * @param {String} string
 * @param {number[][]} matrix
 * @param {boolean[][]} visitedMatrix
 * @param {number} row
 * @param {number} col
 * @param {number} rowMax
 * @param {number} colMax
 * @param {number} charAt
 */
const search = (
  string,
  matrix,
  visitedMatrix,
  row,
  col,
  rowMax,
  colMax,
  charAt
) => {
  // If a character doesn't match, return false
  if (matrix[row][col] !== string.charAt(charAt)) return false;

  // If the string is completely matched, return true
  if (charAt === string.length - 1) return true;

  visitedMatrix[row][col] = true;
  // For each direction, traverse in that direction if conditions are valid
  for (let i = 0; i < 4; i++) {
    let newRow = row + directions[i][0];
    let newCol = col + directions[i][1];

    if (
      isWithInBounds(newRow, newCol, rowMax, colMax) &&
      !visitedMatrix[newRow][newCol]
    ) {
      if (
        search(
          string,
          matrix,
          visitedMatrix,
          newRow,
          newCol,
          rowMax,
          colMax,
          charAt + 1
        )
      ) {
        return true;
      }
    }
  }

  visitedMatrix[row][col] = false;
  return false;
};

/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function (board, word) {
  let returnValue = false;

  // Create the visited matrix
  let visitedMatrix = new Array(board.length);
  for (let i = 0; i < board.length; i++) {
    visitedMatrix[i] = new Array(board[0].length);
  }

  for (let row = 0; row < board.length; row++) {
    for (let col = 0; col < board[0].length; col++) {
      if (
        search(
          word,
          board,
          visitedMatrix,
          row,
          col,
          board.length,
          board[0].length,
          0
        )
      ) {
        returnValue = true;
        break;
      }
    }
  }

  return returnValue;
};

console.log(
  exist(
    [
      ['A', 'B', 'C', 'E'],
      ['S', 'F', 'C', 'S'],
      ['A', 'D', 'E', 'E'],
    ],
    'ABCCED'
  )
);
console.log(
  exist(
    [
      ['A', 'B', 'C', 'E'],
      ['S', 'F', 'C', 'S'],
      ['A', 'D', 'E', 'E'],
    ],
    'SEE'
  )
);
console.log(
  exist(
    [
      ['A', 'B', 'C', 'E'],
      ['S', 'F', 'C', 'S'],
      ['A', 'D', 'E', 'E'],
    ],
    'ABCB'
  )
);
