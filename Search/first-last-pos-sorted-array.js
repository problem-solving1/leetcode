let search = function (nums, target, start, end, indices) {
  if (start > end) return;

  let mid = start + Math.floor((end - start) / 2);

  //if match found
  if (nums[mid] === target) {
    // updating start position
    if (indices[0] === -1 || indices[0] > mid) {
      indices[0] = mid;
    }
    // updating end position
    if (indices[1] === -1 || indices[1] < mid) {
      indices[1] = mid;
    }

    // searching left
    search(nums, target, start, mid - 1, indices);
    // searching right
    search(nums, target, mid + 1, end, indices);
  } else if (nums[mid] < target) {
    search(nums, target, mid + 1, end, indices);
  } else {
    search(nums, target, start, mid - 1, indices);
  }
};

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var searchRange = function (nums, target) {
  let indices = [-1, -1];
  search(nums, target, 0, nums.length - 1, indices);
  // console.log(indices);
  return indices;
};

searchRange([5, 7, 7, 8, 8, 10], 6);

// Time complexity: O(logN)
