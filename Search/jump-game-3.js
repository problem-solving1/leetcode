const helper = (arr, start, set) => {
  if (arr[start] === 0) {
    return true;
  } else if (set.has(start) || start < 0 || start > arr.length) {
    return false;
  }

  set.add(start);

  if (
    helper(arr, start - arr[start], set) ||
    helper(arr, start + arr[start], set)
  ) {
    return true;
  }

  set.delete(start);
};

/**
 * @param {number[]} arr
 * @param {number} start
 * @return {boolean}
 */
var canReach = function (arr, start) {
  let set = new Set();
  return helper(arr, start, set) || false;
};

// Search: DFS O(N)
