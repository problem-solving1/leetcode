'use strict';

const AVL = (function () {
  let weakMap = new WeakMap();

  const Node = function (data) {
    (this.data = data),
      (this.left = null),
      (this.right = null),
      (this.height = 0);
  };

  const AVL = function () {
    weakMap.set(this, null);
  };

  const getHeight = (node) => {
    if (!node) {
      return -1;
    } else {
      return node.height;
    }
  };

  const updateHeight = (node) => {
    if (!node.left && !node.right) node.height = 0;
    else {
      node.height =
        Math.max(node.left?.height ?? null, node.right?.height ?? null) + 1;
    }
  };

  const singleRotateLeft = function (node) {
    let newCentralNode = node.left;
    node.left = newCentralNode.right;
    newCentralNode.right = node;

    // newCentralNode.height =
    //   Math.max(
    //     0,
    //     newCentralNode.left?.height ?? null,
    //     newCentralNode.right?.height ?? null
    //   ) + 1;

    updateHeight(node);

    return newCentralNode;
  };

  const singleRotateRight = function (node) {
    let newCentralNode = node.right;
    node.right = newCentralNode.left;
    newCentralNode.left = node;

    // newCentralNode.height =
    //   Math.max(
    //     0,
    //     newCentralNode.left?.height ?? null,
    //     newCentralNode.right?.height ?? null
    //   ) + 1;

    updateHeight(node);

    return newCentralNode;
  };

  const doubleRotateLeft = function (node) {
    node.left = singleRotateRight(node.left);
    return singleRotateLeft(node);
  };

  const doubleRotateRight = function (node) {
    node.right = singleRotateLeft(node.right);
    return singleRotateRight(node);
  };

  const insertHelper = function (root, data) {
    if (!root) {
      root = new Node(data);
    } else if (data < root.data) {
      root.left = insertHelper(root.left, data);

      if (getHeight(root.left) - getHeight(root.right) === 2) {
        if (data < root.left.data) {
          root = singleRotateLeft(root);
        } else {
          root = doubleRotateLeft(root);
        }
      }
    } else if (data >= root.data) {
      root.right = insertHelper(root.right, data);

      if (getHeight(root.right) - getHeight(root.left) === 2) {
        if (data > root.right.data) {
          root = singleRotateRight(root);
        } else {
          root = doubleRotateRight(root);
        }
      }
    }

    updateHeight(root);

    return root;
  };

  AVL.prototype.insert = function (data) {
    let root = weakMap.get(this);
    root = insertHelper(root, data);
    weakMap.set(this, root);
  };

  // goes one node left and then all nodes right
  const findSuccessor = (root) => {
    let tempNode = root.left;
    while (tempNode.right) {
      tempNode = tempNode.right;
    }

    return tempNode;
  };

  const getBalanceFactor = (node) => {
    return getHeight(node.left) - getHeight(node.right);
  };

  const deleteHelper = function (root, data) {
    // navigating the node to delete
    if (!root) return null;
    else if (root.data === data) {
      if (root.left && root.right) {
        let successorNode = findSuccessor(root);
        root.data = successorNode.data;
        root.left = deleteHelper(root.left, successorNode.data);
      } else if (root.left) {
        root = root.left;
      } else if (root.right) {
        root = root.right;
      } else {
        root = null;
      }

      return root;
    } else if (data < root.data) {
      root.left = deleteHelper(root.left, data);
    } else {
      root.right = deleteHelper(root.right, data);
    }

    // checking node balance and updating balance
    if (getBalanceFactor(root) === 2) {
      if (getBalanceFactor(root.left) >= 0) {
        root = singleRotateLeft(root);
      } else {
        root = doubleRotateLeft(root);
      }
    } else if (getBalanceFactor(root) === -2) {
      if (getBalanceFactor(root.right) <= 0) {
        root = singleRotateRight(root);
      } else {
        root = doubleRotateRight(root);
      }
    }

    updateHeight(root);
    return root;
  };

  AVL.prototype.delete = function (data) {
    let root = weakMap.get(this);
    root = deleteHelper(root, data);
    weakMap.set(this, root);
  };

  AVL.prototype.searchNearest = function (data) {
    let closestIndex = null;
    let min = Number.MAX_SAFE_INTEGER;

    let root = weakMap.get(this);

    while (root !== null) {
      if (root.data < data) {
        root = root.right;
      } else if (root.data > data) {
        if (root.data - data < min) {
          min = root.data - data;
          closestIndex = root.data;
        }
        root = root.left;
      }
    }

    return closestIndex;
  };

  const inOrderHelper = function (root, array) {
    if (!root) return;
    inOrderHelper(root.left, array);
    array.push(root.data);
    inOrderHelper(root.right, array);
  };

  AVL.prototype.inOrderTraversal = function () {
    let root = weakMap.get(this);
    let array = [];
    inOrderHelper(root, array);
    return array;
  };

  AVL.prototype.print = function () {
    let root = weakMap.get(this);
    if (!root) return;

    let queue = [];

    queue.push(root);
    while (queue.length > 0) {
      let top = queue.shift();

      if (top.left) queue.push(top.left);
      if (top.right) queue.push(top.right);

      console.log(top.data);
    }
  };

  return AVL;
})();

/**
 * @param {number[]} rains
 * @return {number[]}
 */
var avoidFlood = function (rains) {
  // stores the days when it did not rain
  let dryDays = new AVL();

  // stores the ponds and the last day it rained over them
  let rainDaysWithPond = new Map();

  let result = new Array(rains.length);

  for (let i = 0; i < rains.length; i++) {
    // if it rained on a pond today
    if (rains[i] !== 0) {
      result[i] = -1;

      // if it did not rain previously on the same pond
      if (!rainDaysWithPond.has(rains[i])) {
        // store the pond and the day it rained over it
        rainDaysWithPond.set(rains[i], i);
      } else {
        // get the closest day it did not rain (next to the day it rained on a pond)
        let closestNextDryDay = dryDays.searchNearest(
          rainDaysWithPond.get(rains[i])
        );

        // if there's a dry day after it rained over the pond
        if (closestNextDryDay) {
          dryDays.delete(closestNextDryDay);
          // empty the pond
          result[closestNextDryDay] = rains[i];
          // set i as the day it rained over the pond
          rainDaysWithPond.set(rains[i], i);
        } else {
          result = [];
          break;
        }
      }
    } else {
      dryDays.insert(i);
    }
  }

  if (result.length > 0) {
    let remainingDryDays = dryDays.inOrderTraversal();
    remainingDryDays.forEach((dryDay) => {
      result[dryDay] = 1;
    });
  }

  return result;
};

// debugger;
console.log(avoidFlood([0, 1, 1]));
