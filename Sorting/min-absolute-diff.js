/**
 * @param {number[]} arr
 * @return {number[][]}
 */
var minimumAbsDifference = function (arr) {
  arr.sort((a, b) => {
    if (a < b) return -1;
    else if (a > b) return 1;
    else return 0;
  });

  var min = Number.MAX_SAFE_INTEGER;
  var length = arr.length;

  for (var i = 1; i < length; i++) {
    min = arr[i] - arr[i - 1] < min ? arr[i] - arr[i - 1] : min;
  }

  var pairs = [];
  for (var i = 1; i < length; i++) {
    if (arr[i] - arr[i - 1] === min) pairs.push([arr[i - 1], arr[i]]);
  }

  return pairs;
};

// console.log(minimumAbsDifference([4, 2, 1, 3]));
// console.log(minimumAbsDifference([1, 3, 6, 10, 15]));
// console.log(minimumAbsDifference([3, 8, -10, 23, 19, -4, -14, 27]));
