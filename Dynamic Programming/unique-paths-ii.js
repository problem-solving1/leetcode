/**
 * Uses DP solution. Time complexity is O(N).
 * @param {number[][]} obstacleGrid
 * @return {number}
 */
var uniquePathsWithObstacles = function (obstacleGrid) {
    let maxRow = obstacleGrid.length - 1;
    let maxCol = obstacleGrid[0].length - 1;

    // if start or end has a block in it, return 0
    if (obstacleGrid[maxRow][maxCol] === 1 || obstacleGrid[0][0] === 1) {
        return 0;
    }

    // calculate the paths possible
    for (let row = 0; row <= maxRow; row++) {
        for (let col = 0; col <= maxCol; col++) {
            // for start position, mark it reachable
            if (row === 0 && col === 0) {
                obstacleGrid[row][col] = 1;
                continue;
            } else if (obstacleGrid[row][col] === 1) {
                // for block in paths, mark position as unreachable
                obstacleGrid[row][col] = 0;
                continue;
            }

            let up = row - 1 < 0 || obstacleGrid[row - 1][col] === 0 ? 0 : obstacleGrid[row - 1][col];
            let left = col - 1 < 0 || obstacleGrid[row][col - 1] === 0 ? 0 : obstacleGrid[row][col - 1];

            obstacleGrid[row][col] = up + left;
        }
    }

    return obstacleGrid[maxRow][maxCol];
};

console.log(uniquePathsWithObstacles([[0, 0, 0], [0, 1, 0], [0, 0, 0]]));
console.log(uniquePathsWithObstacles([[0, 1], [0, 0]]));
console.log(uniquePathsWithObstacles([[0, 0, 1], [0, 0, 0], [0, 0, 0]]));
console.log(uniquePathsWithObstacles([[0]]));
console.log(uniquePathsWithObstacles([[0, 1]]));
console.log(uniquePathsWithObstacles([[1, 0]]));
console.log(uniquePathsWithObstacles([[1], [0]]));