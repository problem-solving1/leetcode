// O(N) solution
// When the equation |x - y| = k is solved, we need to check if x+k and x-k exists in the array, and for each, increment count.

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findPairs = function (nums, k) {
  let map = new Map();
  let count = 0;

  nums.forEach((num) => {
    if (k === 0 && map.get(num) === 1) {
      count++;
      map.set(num, 2);
    }

    if (map.has(num)) return;

    if (map.has(num + k)) {
      count++;
    }

    if (map.has(num - k)) {
      count++;
    }

    if (!map.has(num)) map.set(num, 1);
    else map.set(num, map.get(num) + 1);
  });

  return count;
};

console.log(findPairs([3, 1, 4, 1, 5], 2));
console.log(findPairs([1, 2, 3, 4, 5], 1));
// debugger;
console.log(findPairs([1, 3, 1, 5, 4], 0));
console.log(findPairs([1, 2, 4, 4, 3, 3, 0, 9, 2, 3], 3));
console.log(findPairs([-1, -2, -3], 1));
