/**
 * Time complexity is O(m*n) where m is the number of friendships and n the number of languages.
 * Space complexity is O(m*n) for forming the peopleToLanguage map.
 * @param {number} n
 * @param {number[][]} languages
 * @param {number[][]} friendships
 * @return {number}
 */
var minimumTeachings = function (n, languages, friendships) {
    let peopleToLanguageMap = new Map();

    // Map which represents the language a person knows.
    languages.forEach((language, index) => {
        peopleToLanguageMap.set(index + 1, new Set(language));
    })

    // Omit friendships that speak a common language
    let differentLanguageFriends = [];
    friendships.forEach(friendship => {
        let person1 = friendship[0];
        let person2 = friendship[1];

        let languageSetPerson2 = peopleToLanguageMap.get(person2);
        let languageArrayPerson1 = languages[person1 - 1];

        let speakSimilar = false;
        for (let i = 0; i < languageArrayPerson1.length; i++) {
            if (languageSetPerson2.has(languageArrayPerson1[i])) {
                speakSimilar = true;
                break;
            }
        }

        if (!speakSimilar) {
            differentLanguageFriends.push(friendship);
        }
    })

    let languageToPeopleMap = new Map();
    let peopleSet = new Set();
    friendships = differentLanguageFriends;
    // For all languages, add people who speak it to corresponding set.
    for (let i = 1; i <= n; i++) {
        languageToPeopleMap.set(i, new Set());
        friendships.forEach(friendship => {
            let person1 = friendship[0];
            let person2 = friendship[1];

            peopleSet.add(person1).add(person2);

            // If person1 speaks the language, add that person to the people set for the language
            if (peopleToLanguageMap.get(person1).has(i)) {
                languageToPeopleMap.get(i).add(person1);
            }

            // If person1 speaks the language, add that person to the people set for the language
            if (peopleToLanguageMap.get(person2).has(i)) {
                languageToPeopleMap.get(i).add(person2);
            }
        })
    }

    // Find the language most used
    let max = 0;
    for (let key of languageToPeopleMap.keys()) {
        let size = languageToPeopleMap.get(key).size;
        if (size > max) max = size;
    }

    return peopleSet.size - max;
};

console.log(minimumTeachings(2, [[1], [2], [1, 2]], [[1, 2], [1, 3], [2, 3]]));
console.log(minimumTeachings(3, [[2], [1, 3], [1, 2], [3]], [[1, 4], [1, 2], [3, 4], [2, 3]]));
