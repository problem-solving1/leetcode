
/**
 * 
 * @param {number} length
 */
var SnapshotArray = function (length) {
  this.map = new Map();
  this.changes = new Map();
  this.snapCount = 0;
};

// Time complexity: O(log(length)), where length is the number of times the value at an index has been changed
SnapshotArray.prototype.binarySearch = (array, data) => {
  let start = 0;
  let end = array.length - 1;
  let result = Number.MIN_VALUE;

  while (start <= end) {
    let mid = Math.floor(start + (end - start) / 2);
    if (array[mid][0] === data) {
      result = array[mid][1];
      break;
    } else if (data > array[mid][0]) {
      start = mid + 1;
      result = array[mid][1];
    } else {
      end = mid - 1;
    }
  }

  return result;
};

/**
 * Time complexity: O(1)
 * @param {number} index
 * @param {number} val
 * @return {void}
 */
SnapshotArray.prototype.set = function (index, val) {
  this.changes.set(index, val);
};

/**
 * Time complexity: O(changes.size), the number of times a change has been made before a snap
 * @return {number}
 */
SnapshotArray.prototype.snap = function () {
  // traverse through the changes -> (index, value) map
  for (let key of this.changes.keys()) {
    // for each key(index), create (snapCount, value) subarray
    let subArray = [this.snapCount, this.changes.get(key)];
    // if key exists, put the data in map
    if (this.map.has(key)) {
      this.map.get(key).push(subArray);
    } else {
      // else initialize an array inside of map and then push the data
      this.map.set(key, [subArray]);
    }
  }

  // reset the changes map
  this.changes.clear();

  return this.snapCount++;
};

/**
 * @param {number} index
 * @param {number} snap_id
 * @return {number}
 */
SnapshotArray.prototype.get = function (index, snap_id) {
  let array = this.map.get(index);
  if(!array) return 0;
  return this.binarySearch(array, snap_id);
};

var obj = new SnapshotArray(3);
// debugger;
obj.set(0, 1);
var param_2 = obj.snap();
console.log(param_2);
var param_3 = obj.get(0, 0);
console.log(param_3);
obj.set(0, 4);
obj.set(1, 4);
obj.set(2, 5);
console.log(obj.snap());
console.log(obj.get(0, 0), obj.get(0, 1), obj.get(2, 1));