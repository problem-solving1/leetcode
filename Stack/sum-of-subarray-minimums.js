/**
 * O(N) time complexity by using Monotone Stack
 * @param {number[]} arr
 * @return {number}
 */
var sumSubarrayMins = function (arr) {
  let nextSmallest = [];
  let prevSmallest = [];

  let stack = [];
  // finding immediate least number index behind each element
  for (let i = 0; i < arr.length; i++) {
    while (stack.length > 0 && arr[i] <= arr[stack[stack.length - 1]]) {
      stack.pop();
    }

    if (stack.length === 0) {
      prevSmallest[i] = -1;
    } else {
      prevSmallest[i] = stack[stack.length - 1];
    }

    stack.push(i);
  }

  stack = [];
  // finding immediate least number index after each element
  for (let i = arr.length - 1; i >= 0; i--) {
    while (stack.length > 0 && arr[i] < arr[stack[stack.length - 1]]) {
      stack.pop();
    }

    if (stack.length === 0) {
      nextSmallest[i] = -1;
    } else {
      nextSmallest[i] = stack[stack.length - 1];
    }

    stack.push(i);
  }

  let sum = 0;
  let distanceAfter = 0;
  let distanceBefore = 0;
  for (let i = 0; i < arr.length; i++) {
    if (nextSmallest[i] === -1) {
      distanceAfter = arr.length - i;
    } else {
      distanceAfter = nextSmallest[i] - i;
    }

    distanceBefore = i - prevSmallest[i];

    sum += arr[i] * distanceAfter * distanceBefore;
    sum %= 1000000007;
  }

  return sum;
};

console.log(sumSubarrayMins([3, 1, 2, 4]));
console.log(sumSubarrayMins([71, 55, 82, 55]));
