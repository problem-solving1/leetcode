const increment = (nums, index) => {
  index++;
  while (nums[index] === nums[index - 1]) {
    index++;
  }

  return index;
};

const decrement = (nums, index) => {
  index--;
  while (nums[index] === nums[index + 1]) {
    index--;
  }

  return index;
};

var fourSum = function (nums, target) {
  nums.sort(function (a, b) {
    if (a < b) {
      return -1;
    } else if (a > b) {
      return 1;
    } else {
      return 0;
    }
  });

  let answer = [];
  let i = 0,
    j;
  while (i <= nums.length - 4) {
    j = i + 1;
    while (j <= nums.length - 3) {
      let start = j + 1;
      let end = nums.length - 1;

      while (start < end) {
        if (nums[start] + nums[end] > target - nums[i] - nums[j]) {
          end = decrement(nums, end);
        } else if (nums[start] + nums[end] < target - nums[i] - nums[j]) {
          start = increment(nums, start);
        } else {
          answer.push([nums[i], nums[j], nums[start], nums[end]]);
          start = increment(nums, start);
          end = decrement(nums, end);
        }
      }

      j = increment(nums, j);
    }

    i = increment(nums, i);
  }

  return answer;
};

console.log(fourSum([0, 0, 0, 0], 0));
