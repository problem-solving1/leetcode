/**
 * @param {number[]} array
 */
const findLeftMostPeak = (array) => {
  let peakIndex = 0;
  while (
    peakIndex + 1 < array.length &&
    array[peakIndex + 1] >= array[peakIndex]
  ) {
    peakIndex++;
  }

  return peakIndex;
};

/**
 * @param {number[]} array
 */
const findRightMostTrough = (array) => {
  let troughIndex = array.length - 1;
  while (troughIndex >= 0 && array[troughIndex] >= array[troughIndex - 1]) {
    troughIndex--;
  }

  return troughIndex;
};

/**
 * O(N) time complexity
 * @param {number[]} arr
 * @return {number}
 */
var findLengthOfShortestSubarray = function (arr) {
  // debugger;
  let left = findLeftMostPeak(arr);
  let right = findRightMostTrough(arr);

  if (right <= left) return 0;

  let elemsToRemove = Math.min(arr.length - left - 1, right);

  let i = 0,
    j = right;
  while (i <= left && j < arr.length) {
    while (arr[j] < arr[i]) {
      j++;
    }
    elemsToRemove = Math.min(elemsToRemove, j - i - 1);
    if (arr[i] <= arr[j]) {
      i++;
    }
  }

  return elemsToRemove;
};

console.log(findLengthOfShortestSubarray([1, 2, 3, 10, 4, 2, 3, 5]));
console.log(findLengthOfShortestSubarray([5, 4, 3, 2, 1]));
console.log(findLengthOfShortestSubarray([1, 2, 3]));
console.log(findLengthOfShortestSubarray([1]));
console.log(findLengthOfShortestSubarray([2, 2, 2, 1, 1, 1]));
console.log(findLengthOfShortestSubarray([1, 2, 3, 10, 0, 7, 8, 9]));
console.log(
  findLengthOfShortestSubarray([6, 3, 10, 11, 15, 20, 13, 3, 18, 12])
);
