var twoSum = function (nums, target) {
  let indices = {};
  let count = {};

  for (let i = 0; i < nums.length; i++) {
    if (!indices[nums[i]]) {
      indices[nums[i]] = [i];
      count[nums[i]] = 1;
    } else {
      indices[nums[i]].push(i);
      count[nums[i]] += 1;
    }
  }

  // debugger;

  for (let i = 0; i < nums.length; i++) {
    let remaining = target - nums[i];
    if (count[remaining]) {
      if (remaining === nums[i]) {
        if (count[remaining] === 1) continue;
        return [indices[nums[i]][0], indices[nums[i]][1]];
      } else {
        return [indices[nums[i]][0], indices[remaining][0]];
      }
    }
  }
};

console.log(twoSum([3,3], 6));
