/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var threeSumClosest = function (nums, target) {
  let min = Number.MAX_SAFE_INTEGER;
  let minSum;

  nums.sort((a, b) => {
    if (a < b) return -1;
    else if (a > b) return 1;
    else return 0;
  });

  // for every number
  for (let i = 0; i < nums.length; i++) {
    // debugger;
    // let newTarget = Math.abs(target - nums[i]);
    let newTarget = target - nums[i];
    // set two pointers
    let start = i + 1;
    let end = nums.length - 1;
    while (start < end) {
      let sumOfPointers = nums[start] + nums[end];
      let gap = Math.abs(newTarget - sumOfPointers);

      if (gap < min) {
        min = gap;
        minSum = sumOfPointers + nums[i];
      }

      if (sumOfPointers <= newTarget) {
        start++;
      } else {
        end--;
      }
    }
  }

  return minSum;
};

console.log(threeSumClosest([-1, 2, 1, -4], 1));
