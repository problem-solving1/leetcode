/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortedSquares = function (A) {
  var sortedArray = [];
  var start = 0,
    end = A.length - 1;

  while (start <= end) {
    var startValue = A[start] * A[start];
    var endValue = A[end] * A[end];
    if (startValue < endValue) {
      sortedArray.unshift(endValue);
      end--;
    } else {
      sortedArray.unshift(startValue);
      start++;
    }
  }

  return sortedArray;
};

console.log(sortedSquares([-4, -1, 0, 3, 10]));
console.log(sortedSquares([-7, -3, 2, 3, 11]));
