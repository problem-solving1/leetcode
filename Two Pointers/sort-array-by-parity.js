/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortArrayByParity = function (A) {
  var start = 0;
  var end = A.length - 1;

  while (start < end) {
    // increment start while start is even
    while (start < end && A[start] % 2 === 0) {
      start++;
    }

    // decrement end while end is odd
    while (start < end && A[end] % 2 !== 0) {
      end--;
    }

    if (start < end) {
      [A[start], A[end]] = [A[end], A[start]];
      start++;
      end--;
    }
  }

  return A;
};
