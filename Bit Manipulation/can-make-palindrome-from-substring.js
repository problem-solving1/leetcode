/**
 * O(N) solution using bits.
 * @param {string} s
 * @param {number[][]} queries
 * @return {boolean[]}
 */
var canMakePaliQueries = function (s, queries) {

    let prefixArray = [0];

    // build prefix sum array
    for (let i = 1; i <= s.length; i++) {
        // clone previous prefix index and update count
        let prevOddPositions = prefixArray[i - 1];
        let currentOddPosition = 1 << (s.charCodeAt(i-1) - 'a'.charCodeAt(0));
        prefixArray.push(prevOddPositions ^ currentOddPosition);
    }

    let result = [];

    for (let [start, end, swapCount] of queries) {

        let oddBitPositions = prefixArray[end + 1] ^ prefixArray[start];
        let oddCount = 0;
        for (let i=1; i<=26; i++) {
            if(oddBitPositions & 1) oddCount++;
            oddBitPositions = oddBitPositions >>> 1;
        }

        result.push(Math.floor(oddCount / 2) <= swapCount);
    }

    return result;
};

console.log(canMakePaliQueries("abcda", [[3, 3, 0], [1, 2, 0], [0, 3, 1], [0, 3, 2], [0, 4, 1]]));