const isDuplicatePresentOrDigitInvalid = (grid, row, col) => {
  let countArray = [];
  for (let r = 0; r < 3; r++) {
    for (let c = 0; c < 3; c++) {
    if (
      countArray[grid[row + r][col + c]] ||
        grid[row + r][col + c] > 9 ||
        grid[row + r][col + c] < 1
      ) {
        return true;
      } else {
        countArray[grid[row + r][col + c]] = 1;
      }
    }
  }

  return false;
};

const allSumEqual = (grid, row, col, diagonal) => {
  let isSumEqual = true;
  for (let i = 0; i < 2; i++) {
    let rowSum =
      grid[row + i][col] + grid[row + i][col + 1] + grid[row + i][col + 2];
    let colSum =
      grid[row][col + i] + grid[row + 1][col + i] + grid[row + 2][col + i];

    if (rowSum !== colSum || rowSum !== diagonal) {
      isSumEqual = false;
      break;
    }
  }

  return isSumEqual;
};

/**
 * Time complexity: O((m-3+1)*(n-3+1)*9), where m = rows of grid, n = cols of grid
 * @param {number[][]} grid
 * @return {number}
 */
var numMagicSquaresInside = function (grid) {
  let count = 0;
  // for all 3x3 grid
  for (let row = 0; row + 2 < grid.length; row++) {
    for (let col = 0; col + 2 < grid[0].length; col++) {
      // check if all digits are between 1 and 9 inclusive and no duplicate digits exist
      if (!isDuplicatePresentOrDigitInvalid(grid, row, col)) {
        /* check if diagonals are equal and sum of each
        row and each col equal sum of any one diagonal */
        let diagonal1 =
          grid[row][col] + grid[row + 1][col + 1] + grid[row + 2][col + 2];
        let diagonal2 =
          grid[row][col + 2] + grid[row + 1][col + 1] + grid[row + 2][col];
        if (diagonal1 === diagonal2 && allSumEqual(grid, row, col, diagonal1)) {
          count++;
        }
      }
    }
  }

  return count;
};

// console.log(
// numMagicSquaresInside([
// [10, 3, 5],
// [1, 6, 11],
// [7, 9, 2],
// ])
// );

// console.log(
// numMagicSquaresInside([
// [4, 3, 8, 4],
// [9, 5, 1, 9],
// [2, 7, 6, 2],
// ])
// );

// console.log(numMagicSquaresInside([[[8]]]));

// console.log(
// numMagicSquaresInside([
// [4, 4],
// [3, 3],
// ])
// );

// console.log(
// numMagicSquaresInside([
// [4, 7, 8],
// [9, 5, 1],
// [2, 3, 6],
// ])
// );
