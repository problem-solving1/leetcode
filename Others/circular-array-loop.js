const modulo = (distance, length) => {
  return ((distance % length) + length) % length;
};

const getNextIndex = (currentIndex, distance, length) => {
  return (currentIndex + modulo(distance, length)) % length;
};

/**
 * @param {number[]} array
 * @param {number[]} queue
 * @param {number} initialIndex
 * @param {number} currentIndex
 */
const jump = (array, queue, currentIndex) => {
  if (queue) queue.push(currentIndex);
  let distanceToJump = array[currentIndex];
  let nextIndex = getNextIndex(currentIndex, distanceToJump, array.length);

  /* if next index falls in:
    already failed sequence || bi-directional flow || is currentIndex
    */
  if (
    array[nextIndex] === 0 ||
    array[nextIndex] * distanceToJump < 0 ||
    nextIndex === currentIndex
  ) {
    return -1;
  }

  return nextIndex;
};

const doubleJump = (array, currentIndex) => {
  let index = jump(array, undefined, currentIndex);
  if (index < 0) {
    return index;
  }

  return jump(array, undefined, index);
};

/**
 * @param {number[]} nums
 * @return {boolean}
 */
var circularArrayLoop = function (nums) {
  let hasCycle = false;
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === 0) continue;

    let queue = [];
    let a, b;
    (a = i), (b = i);
    do {
      a = jump(nums, queue, a);
      b = doubleJump(nums, b);
      if (a < 0 || b < 0) {
        while (queue.length > 0) {
          nums[queue.pop()] = 0;
        }
        break;
      }
    } while (a !== b);

    if (a === b && a >= 0 && b >= 0 && queue.length > 1) {
      hasCycle = true;
      break;
    }
  }

  return hasCycle;
};

// debugger;
// console.log(circularArrayLoop([2, -1, 1, 2, 2]));
// console.log(circularArrayLoop([-1, 2]));
// console.log(circularArrayLoop([2, 1, -1, -2, -2]));
// console.log(circularArrayLoop([3, 1, 2]));
// console.log(circularArrayLoop([1, 1, 2]));
console.log(circularArrayLoop([2, 2, 2, 2, 2, 4, 7]));

// console.log(modulo(-1, 5));
