/**
 *
 * @param {number[]} horizontalCuts
 * @param {number} height
 */
const getHeight = (horizontalCuts, height) => {
  let max = Number.MIN_SAFE_INTEGER;

  if (horizontalCuts.length === 0) {
    max = height;
  } else if (horizontalCuts.length === 1) {
    max = Math.max(horizontalCuts[0], height - horizontalCuts[0]);
  } else {
    for (let i = 1; i < horizontalCuts.length; i++) {
      let gap = Math.abs(horizontalCuts[i] - horizontalCuts[i - 1]);
      if (gap > max) {
        max = gap;
      }
    }
  }

  return Math.max(
    max,
    horizontalCuts[0],
    height - horizontalCuts[horizontalCuts.length - 1]
  );
};

/**
 *
 * @param {number[]} verticalCuts
 * @param {number} width
 */
const getWidth = (verticalCuts, width) => {
  let max = Number.MIN_SAFE_INTEGER;

  if (verticalCuts.length === 0) {
    max = width;
  } else if (verticalCuts.length === 1) {
    max = Math.max(verticalCuts[0], width - verticalCuts[0]);
  } else {
    for (let i = 1; i < verticalCuts.length; i++) {
      let gap = Math.abs(verticalCuts[i] - verticalCuts[i - 1]);
      if (gap > max) {
        max = gap;
      }
    }
  }

  return Math.max(
    max,
    verticalCuts[0],
    width - verticalCuts[verticalCuts.length - 1]
  );
};

const comparator = (a, b) => {
  return a - b;
};

/**
 * @param {number} h
 * @param {number} w
 * @param {number[]} horizontalCuts
 * @param {number[]} verticalCuts
 * @return {number}
 */
var maxArea = function (h, w, horizontalCuts, verticalCuts) {
  horizontalCuts.sort(comparator);
  verticalCuts.sort(comparator);
  return (
    (getHeight(horizontalCuts, h) * getWidth(verticalCuts, w)) % 1000000007
  );
};

// console.log(maxArea(5, 4, [1, 2, 4], [1, 3]));
// console.log(maxArea(5, 4, [3, 1], [1]));
// console.log(maxArea(5, 4, [3], [3]));
// debugger;
// console.log(maxArea(8, 5, [5, 2, 6, 3], [1, 4]));
// console.log(maxArea(5, 2, [3, 1, 2], [1]));
