/**
 *
 * @param {TreeNode} currentNode
 * @param {number[]} array
 */
const bstToSortedArray = (currentNode, array) => {
  if (!currentNode) return;

  bstToSortedArray(currentNode.left, array);
  array.push(currentNode.val);
  bstToSortedArray(currentNode.right, array);
};

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {boolean}
 */
var findTarget = function (root, k) {
  let array = [];
  bstToSortedArray(root, array);

  let start = 0;
  let end = array.length - 1;
  while (start < end) {
    let sum = array[start] + array[end];
    if (sum < k) {
      start++;
    } else if (sum > k) {
      end--;
    } else {
      return true;
    }
  }

  return false;
};
