/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
  debugger;
  let queue = [];
  nums.forEach((number, numberIndex) => {
    if (number !== 0 && queue.length !== 0) {
      let index = queue.shift();
      nums[index] = number;
      queue.push(numberIndex);
    } else if (number === 0) {
      queue.push(numberIndex);
    }
  });

  queue.forEach((index) => {
    if (nums[index] !== 0) {
      nums[index] = 0;
    }
  });

  // return nums;
};

console.log(moveZeroes([1]));
