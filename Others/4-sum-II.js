/**
 * @param {number[]} A
 * @param {number[]} B
 * @param {number[]} C
 * @param {number[]} D
 * @return {number}
 */
var fourSumCount = function (A, B, C, D) {
  let myMap = new Map();

  for (let i = 0; i < A.length; i++) {
    for (let j = 0; j < B.length; j++) {
      if (!myMap.has(A[i] + B[j])) {
        myMap.set(A[i] + B[j], 1);
      } else {
        myMap.set(A[i] + B[j], myMap.get(A[i] + B[j]) + 1);
      }
    }
  }

  let result = 0;
  for (let k = 0; k < C.length; k++) {
    for (let l = 0; l < D.length; l++) {
      if (myMap.has((C[k] + D[l]) * -1)) {
        result += myMap.get((C[k] + D[l]) * -1);
      }
    }
  }

  return result;
};


console.log(fourSumCount([1, 2], [-2, -1], [-1, 2], [0, 2]));
