/**
 * @param {number[]} nums
 * @return {boolean}
 */
var checkPossibility = function (nums) {
  let maxDescPos = 0;
  for (let i = 1; i < nums.length; i++) {
    // if current element smaller than previous
    if (nums[i] < nums[i - 1]) {
      // check if previous element can be altered
      let isPrevAlterable = i - 1 === 0 || nums[i - 2] <= nums[i];

      // check if current element can be altered
      let isCurrentAlterable =
        i === nums.length - 1 || nums[i - 1] <= nums[i + 1];

      if (isPrevAlterable || isCurrentAlterable) {
        maxDescPos++;
      } else {
        maxDescPos = 100;
        break;
      }
    }
  }

  // console.log(maxDescPos);

  return maxDescPos < 2;
};

console.log(checkPossibility([4, 2, 3, 2]));
console.log(checkPossibility([4, 2]));
console.log(checkPossibility([4, 3, 2]));
console.log(checkPossibility([2, 4, 3, 3]));
console.log(checkPossibility([1, 2, 4, 5, 3]));
