/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function (nums) {
  if (nums.length === 1) return true;

  let zeroIndex = null;

  for (let i = nums.length - 2; i >= 0; i--) {
    // selecting 0's (obstacle) index
    if (nums[i] === 0 && !zeroIndex) {
      zeroIndex = i;
      continue;
    }

    // checking if obstacle can be avoided or not
    // if obstacle can be avoided, reset index of obstacle
    if (nums[i] + i > zeroIndex) {
      zeroIndex = null;
    }
  }

  return zeroIndex === null;
};

console.log(canJump([3, 2, 1, 0, 4]));
