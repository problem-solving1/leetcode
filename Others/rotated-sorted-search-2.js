/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function (nums, target) {
  let ifFound = false;
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === target) {
      ifFound = true;
      break;
    }
  }
  return ifFound;
};

console.log(search([2, 5, 6, 0, 0, 1, 2], 3));
