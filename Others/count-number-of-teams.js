/**
 * @param {number[]} rating
 * @return {number}
 */
var numTeams = function (rating) {
  let before = [];
  let after = [];

  // calculate small and large elements
  for (let i = 1; i < rating.length - 1; i++) {
    // debugger;
    let smallCount = 0;
    let largeCount = 0;

    // calculate small and large before elements
    for (let j = 0; j < i; j++) {
      if (rating[j] < rating[i]) {
        smallCount++;
      } else if (rating[j] > rating[i]) {
        largeCount++;
      }
    }

    before[i] = { smallCount, largeCount };

    smallCount = 0;
    largeCount = 0;

    // calculate small and large after elements
    for (let j = i + 1; j < rating.length; j++) {
      if (rating[j] < rating[i]) {
        smallCount++;
      } else if (rating[j] > rating[i]) {
        largeCount++;
      }
    }

    after[i] = { smallCount, largeCount };
  }

  let total = 0;
  // calculate total tuples
  for (let i = 1; i < rating.length - 1; i++) {
    total += before[i].smallCount * after[i].largeCount;
    total += before[i].largeCount * after[i].smallCount;
  }

  return total;
};

console.log(numTeams([2,1,3]));
