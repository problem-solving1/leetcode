const sortFunction = (a, b) => {
  if (a < b) return -1;
  else if (a > b) return 1;
  else return 0;
};

/**
 * @param {number[]} nums
 * @return {number}
 */
var minDifference = function (nums) {
  if (nums.length <= 4) {
    return 0;
  }

  nums.sort(sortFunction);

  let min = Number.MAX_SAFE_INTEGER;
  for (let start = 0; start <= 3; start++) {
    let end = nums.length - 4 + start;
    min = nums[end] - nums[start] < min ? nums[end] - nums[start] : min;
  }

  return min;
};

// Time complexity O(3)
