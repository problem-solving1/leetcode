var Heap = (() => {
  var weakMap = new WeakMap();
  var Heap = function () {
    weakMap.set(this, []);
  };

  /**
   * @param {interval} i
   */
  Heap.prototype.push = function (i) {
    var array = weakMap.get(this);

    // push interval object in array
    array.push(i);

    // percolate up (priority: distance, index)
    var currentIndex = array.length - 1;
    var parentIndex = Math.floor((currentIndex - 1) / 2);
    var currentNode = array[currentIndex];
    var parentNode = array[parentIndex];
    while (
      currentIndex > 0 &&
      (currentNode.distance > parentNode.distance ||
        (currentNode.distance === parentNode.distance &&
          currentNode.index < parentNode.index))
    ) {
      // swap current node with parent node
      [array[parentIndex], array[currentIndex]] = [
        array[currentIndex],
        array[parentIndex],
      ];

      // update parent index and current index
      currentIndex = parentIndex;
      parentIndex = Math.floor((currentIndex - 1) / 2);
      currentNode = array[currentIndex];
      parentNode = array[parentIndex];
    }
  };

  // checks if parent is smaller than any of child nodes
  var ifParentSmaller = function (array, parentIndex, leftIndex, rightIndex) {
    // getting the nodes
    var parent = array[parentIndex];
    var leftChild = array[leftIndex];
    var rightChild = array[rightIndex];

    var smallerThanLeft =
      leftChild &&
      (parent.distance < leftChild.distance ||
        (parent.distance === leftChild.distance &&
          parent.index > leftChild.index));

    var smallerThanRight =
      rightChild &&
      (parent.distance < rightChild.distance ||
        (parent.distance === rightChild.distance &&
          parent.index > rightChild.index));

    return smallerThanLeft || smallerThanRight;
  };

  // returns greater (based on distance and index) between left and right childs
  var compare = function (array, leftIndex, rightIndex) {
    var leftChild = array[leftIndex];
    var rightChild = array[rightIndex];
    if (
      leftChild.distance > rightChild.distance ||
      (leftChild.distance === rightChild.distance &&
        leftChild.index < rightChild.index)
    ) {
      return leftIndex;
    }

    return rightIndex;
  };

  var percolateDown = function (reference, currentIndex) {
    var array = weakMap.get(reference);

    // calculating the left and right child indices
    var leftIndex = currentIndex * 2 + 1;
    var rightIndex = currentIndex * 2 + 2;

    // while at least one children exists and if parent node smaller than any child node
    while (
      array[leftIndex] &&
      ifParentSmaller(array, currentIndex, leftIndex, rightIndex)
    ) {
      var swapIndex = leftIndex;
      // if right child exists
      if (array[rightIndex]) {
        // get the greater between left and right child
        swapIndex = compare(array, swapIndex, rightIndex);
      }

      // swap appropriate child with parent
      [array[currentIndex], array[swapIndex]] = [
        array[swapIndex],
        array[currentIndex],
      ];

      // point to the swapped position
      currentIndex = swapIndex;
      leftIndex = currentIndex * 2 + 1;
      rightIndex = currentIndex * 2 + 2;
    }
  };

  Heap.prototype.pop = function () {
    var array = weakMap.get(this);

    if (array.length === 0) return -1;

    var top = array[0];
    [array[array.length - 1], array[0]] = [array[0], array[array.length - 1]];
    array.pop();

    percolateDown(this, 0);

    return top;
  };

  Heap.prototype.deactivate = function (seatIndex) {
    var array = weakMap.get(this);

    array.forEach((item) => {
      if (item.start === seatIndex || item.end === seatIndex) {
        item.activeStatus = false;
      }
    });
  };

  Heap.prototype.destroy = function () {
    weakMap.set(this, []);
  };

  Heap.prototype.print = function () {
    console.log(weakMap.get(this));
  };

  return Heap;
})();

// var heap = new Heap();
// heap.push({ distance: 5, index: 3 });
// heap.push({ distance: 15, index: 3 });

// heap.print();

/**
 * @param {number} N
 */
var ExamRoom = function (N) {
  this.heap = new Heap();
  this.array = new Array(N);
  this.arrayElems = 0;
};

const updateArray = function (array, indexToUpdate, leftIndex, rightIndex) {
  if (leftIndex !== undefined) array[indexToUpdate].left = leftIndex;
  if (rightIndex !== undefined) array[indexToUpdate].right = rightIndex;
};

const pushToHeap = function (array, heap, leftIndex, index, rightIndex) {
  if (
    index !== 0 &&
    (array[index - 1] === null || array[index - 1] === undefined)
  ) {
    heap.push({
      start: leftIndex,
      end: index,
      index: Math.floor((leftIndex + index) / 2),
      distance: Math.floor((leftIndex + index) / 2) - leftIndex,
      activeStatus: true,
    });
  }

  if (
    index !== array.length - 1 &&
    (array[index + 1] === null || array[index + 1] === undefined)
  ) {
    heap.push({
      start: index,
      end: rightIndex,
      index: Math.floor((index + rightIndex) / 2),
      distance: Math.floor((index + rightIndex) / 2) - index,
      activeStatus: true,
    });
  }
};

/**
 * @return {number}
 */
ExamRoom.prototype.seat = function () {
  // when no students present, sit at seat 0
  if (this.arrayElems === 0) {
    this.array[0] = { left: null, right: null };
    this.heap.push({
      start: 0,
      end: null,
      index: this.array.length - 1,
      distance: this.array.length - 1,
      activeStatus: true,
    });
    this.arrayElems++;

    return 0;
  }

  var activeStatus = false;
  var interval;
  while (!activeStatus) {
    interval = this.heap.pop();
    activeStatus = interval.activeStatus;
  }

  // insert at index which makes maximum distance
  this.array[interval.index] = { left: null, right: null };

  // update array with inserted interval's left and right seats
  updateArray(this.array, interval.index, interval.start, interval.end);

  // current interval's left seat has new seat (index) on its right
  if (interval.start !== null && interval.start !== undefined)
    updateArray(this.array, interval.start, undefined, interval.index);
  // current interval's right seat has new seat (index) on its left
  if (interval.end !== null && interval.end !== undefined)
    updateArray(this.array, interval.end, interval.index, undefined);

  pushToHeap(
    this.array,
    this.heap,
    interval.start,
    interval.index,
    interval.end
  );

  this.arrayElems++;

  return interval.index;
};

/**
 * @param {number} p
 * @return {void}
 */
ExamRoom.prototype.leave = function (p) {
  if (this.arrayElems === 1) {
    this.heap.destroy();
    this.arrayElems = 0;
    return;
  }

  this.heap.deactivate(p);

  // debugger;
  var interval = this.array[p];
  if (interval.left)
    updateArray(this.array, interval.left, undefined, interval.right);
  if (interval.right)
    updateArray(this.array, interval.right, interval.left, undefined);

  var indexToInsertAt =
    interval.left === null || interval.left === undefined
      ? 0
      : interval.right === null || interval.right === undefined
      ? this.array.length - 1
      : Math.floor((interval.left + interval.right) / 2);

  this.heap.push({
    start: interval.left,
    end: interval.right,
    index: indexToInsertAt,
    distance:
      interval.left === null || interval.left === undefined
        ? interval.right - indexToInsertAt
        : indexToInsertAt - interval.left,
    activeStatus: true,
  });

  this.array[p] = null;
  this.arrayElems--;
};

// var er = new ExamRoom(10);

// // debugger;
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// er.leave(0);
// er.heap.print();
// er.leave(9);
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();
// console.log(er.seat());
// er.heap.print();

module.exports = ExamRoom;
