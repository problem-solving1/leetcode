/**
 * @param {number[][]} intervals
 * @param {number[]} newInterval
 * @return {number[][]}
 */
var insert = function (intervals, newInterval) {
  // variables to track the intervals to remove
  let replaceStart;
  let elementCount = 0;

  intervals.forEach((interval, index) => {
    /* if newInterval is smaller than current interval */
    if (newInterval[1] < interval[0]) {
      return;
    } else if (
      // if newInterval's start within range of an interval
      (newInterval[0] >= interval[0] && newInterval[0] <= interval[1]) ||
      // if newInterval's end within range of an interval
      (newInterval[1] >= interval[0] && newInterval[1] <= interval[1]) ||
      // if an interval falls within newInterval
      (newInterval[0] < interval[0] && newInterval[1] > interval[1])
    ) {
      newInterval[0] = Math.min(newInterval[0], interval[0]);
      newInterval[1] = Math.max(newInterval[1], interval[1]);

      if (replaceStart === undefined) {
        replaceStart = index;
        elementCount = 0;
      }
    }
    elementCount++;
  });

  if (replaceStart === undefined && elementCount === intervals.length) {
    replaceStart = intervals.length;
    elementCount = 0;
  } else if (replaceStart === undefined && elementCount === 0) {
    replaceStart = 0;
  } else if (replaceStart === undefined) {
    replaceStart = elementCount;
    elementCount = 0;
  }

  // insert newInterval and delete necessary intervals
  intervals.splice(replaceStart, elementCount, newInterval);

  return intervals;
};

// console.log(
//   insert(
//     [
//       [1, 2],
//       [3, 5],
//       [6, 7],
//       [8, 10],
//       [12, 16],
//     ],
//     [4, 8]
//   )
// );

// console.log(
//   insert(
//     [
//       [1, 3],
//       [6, 9],
//     ],
//     [2, 5]
//   )
// );

// console.log(insert([], [5, 7]));

// console.log(insert([[1, 5]], [2, 7]));

// console.log(insert([[1, 5]], [6, 8]));

// console.log(
//   insert(
//     [
//       [3, 5],
//       [12, 15],
//     ],
//     [6, 6]
//   )
// );

// console.log(
//   insert(
//     [
//       [1, 5],
//       [6, 8],
//     ],
//     [5, 6]
//   )
// );
